const getCube = Math.pow(2, 3);
console.log(`The cube of 2 is ${getCube}`);

const address = ["258 Washington Ave NW", "California 90011"]
const [country, state] = address
console.log(`I live at ${country}, ${state}`);


const animal = {
	weighed: 1075,
	measurementFt: 20,
	measurementIn: 3
}

const {weighed, measurementFt, measurementIn} = animal

console.log(`Lolong was a saltwater corocodile. He weighed at ${weighed} kgs with a measurement of ${measurementFt} ft ${measurementIn} in.`);

// let reduceNumber = numbers.reduce((x, y) => x + y);
// // (Accumulator , CurrentValue);

// // [1,2,3,4,5]

// // 0 + 1 = 1 - accumulator
// // 1 + 2 = 3 - new accumulator
// // 3 + 3 = 6
// // 6 + 4 = 10
// // 10 + 5 = 15



const numbers = [1,2,3,4,5,6,7];
	numbers.forEach(number => {
	console.log(number);
	})

const reduceNumber = numbers.reduce((total, number) => {
	return total + number
})
console.log(reduceNumber);


class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const dogDetails = new Dog('Mia', 3, 'Askal');
console.log(dogDetails);